A small python script for generating assignments for secret santa!

Note that the assignment generated will result in a single cycle, never a
series of smaller non-connected cycles.

= Usage =
 * Fill up the data.toml document as in the example data.toml, with the
   participants, their address, their interests, and their nicelist,
   somewhat-naughty-list, and very-naughty-list.
 * You will either receive a crash message, requesting you to run the script
   again, or receive a series of .txt files in the same directory as the
   directory you ran the script in. You must now send the .txt file to
   the participant named in the filename of the .txt . That .txt file will
   contain their assigned giftee, including their username, address, interests,
   and a small message thanking them for their participation.

= Nice / Naughty Lists =
* Each participant can provide either a nice-list, a somewhat naughty list, and
  a very naughty list.
    * Nice list: the list of users to which you are comfortable gifting to.
        If not provided, all other participants will initially be put on the
        nice list. Otherwise, the nice list initially contains only users
        explicitly given.
    * Somewhat Naughty List: list of users to be excluded from your nice-list.
      They will be removed from the list of users you are comfortable gifting
      to, but ensures you may still be drawn by them. Useful if you've been
      using this script for multiple years and don't want to draw the same
      person again.
    * Very Naughty: list of users to be excluded from your nice list, and from
      whose nice list you will be excluded from as well. Useful if you don't
      get along with someone.
* The participant names on the above lists must match a username of a
  participant in the participants list.
* If a participant A very-naughty-lists a participant B, participant B cannot
  be either the giftee, nor the gifter, for participant B.
* If a participant A somewhat-naughty-lists a participant B, participant A will
  never draw participant B to gift to, but participant B may end up gifting to
  participant A.

* A very-naughty-list of some participant A always trumps the nice-list of some
  participant B: If A appears on B's whitelist, but B is not on A's whitelist,
  then B will modify its very-naughty-list to add A to his as well.


