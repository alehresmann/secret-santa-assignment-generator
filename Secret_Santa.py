from tomlkit import parse
import random
from time import sleep
import requests
import logging
import sys
from copy import copy

santa_string = r"""
                     __________  __      ____________  ________
                     \______   \/  \    /  \______   \/  _____/
                      |     ___/\   \/\/   /|     ___/   \  ___
                      |    |     \        / |    |   \    \_\  \
                      |____|      \__/\  /  |____|    \______  /
                                       \/                    \/
  _________                            __      _________              __
 /   _____/ ____   ___________   _____/  |_   /   _____/____    _____/  |______
 \_____  \_/ __ \_/ ___\_  __ \_/ __ \   __\  \_____  \\__  \  /    \   __\__  \
 /        \  ___/\  \___|  | \/\  ___/|  |    /        \/ __ \|   |  \  |  / __ \_
/_______  /\___  >\___  >__|    \___  >__|   /_______  (____  /___|  /__| (____  /
        \/     \/     \/            \/               \/     \/     \/          \/

                   *        *        *        __o    *       *
                *      *       *        *    /_| _     *
                   K  *     K      *        O'_)/ \  *    *
                  <')____  <')____    __*   V   \  ) __  *
                   \ ___ )--\ ___ )--( (    (___|__)/ /*     *
                 *  |   |    |   |  * \ \____| |___/ /  *
                    |*  |    |   | aos \____________/       *

"""
# for debugging purposes, so that the organiser isnt spoiled of the users
# drawn in case debugging needs to happen.
list_of_debug_values = "Alatar, Aiwendil, Arien, Curumo, Eönwë, Durin's Bane, Gothmog, Ilmarë, Mairon, Melian, Olórin, Ossë, Pallando, Salmar, Tilion, Uinen, Manwë, Melkor, Ulmo, Aulë, Oromë, Námo, Irmo, Tulkas, Varda, Yavanna, Nienna, Estë, Vairë, Vána, Nessa"
list_of_debug_values = [s.strip() for s in list_of_debug_values.split(",")]


def write_to_file(filename, current_user, next_user):
    f = open(filename, "w")
    f.write(santa_string)
    f.write("Here's the information of the person you are gifting to:\n")
    f.write("USERNAME: " + next_user.name + "\n\n")
    f.write("ADDRESS: " + next_user.address + "\n\n")
    f.write("INTERESTS: " + next_user.interests + "\n\n")
    f.write("DEBUG VALUE (ignore this): " + next_user.debug_value + "\n\n")

    f.write(
        "-----\nThanks a ton for participating, and hope you both find and "
        "receive a wonderful gift!\nMerry Christmas!\n"
    )
    f.write(
        "Here's your debug information. Please double-check this and let me "
        "know if it was wrong!\n"
    )
    f.write("YOUR USERNAME: " + current_user.name + "\n\n")
    f.write("YOUR ADDRESS: " + current_user.address + "\n\n")
    f.write("YOUR INTERESTS: " + current_user.interests + "\n\n")
    f.write("YOUR DEBUG VALUE (ignore this): " + current_user.debug_value + "\n\n")
    # f.write(
    #     "LIST OF USERS YOU COULD HAVE DRAWN: " + str(current_user.can_draw) + "\n\n"
    # )
    f.close()


# the actual user
# can_draw -> set of people you can draw as the person to gift to
class user:
    def __init__(self, name, address, interests, can_draw=[]):

        self.name = name
        self.address = address
        self.interests = interests
        self.can_draw = can_draw
        self.debug_value = None

    def __repr__(self):  # debugging
        return str(
            f"USERNAME:\t{self.name}\n"
            f"ADDRESS:\t{self.address}\n"
            f"INTERESTS:\t{self.interests}\n"
            f"CAN DRAW:\t{self.can_draw}\n"
            f"DEBUG ANIMAL:\t{self.can_draw}\n"
        )


def is_name_given_in_list_of_participants(raw_user, names):
    for raw_list in raw_user[3:6]:
        proper_list = [s.strip() for s in raw_list.split(",")]
        if proper_list == [""]:
            continue
        for name in proper_list:
            if name not in names:
                raise Exception(
                    f"name given in list of {raw_user[0]} is not a participant: {name}"
                )


with open("data.toml", "r") as f:
    raw_data = parse(f.read())["participants"]
    participants = []  # not validated yet.
    for raw_p in raw_data:
        if len(raw_p) != 6:
            raise Exception(
                "The user was incorrectly given! I need exactly the following"
                "nonempty entries: name, address, interests, nice_list, "
                f"somewhat_naughty_list, very_naughty_list. USER: {p}"
            )
        for required in raw_p[:3]:
            if required == "":
                raise Exception(
                    "The user was incorrectly given! I need at least three "
                    f"nonempty entries: name, address, interests. USER: {p}"
                )
        new_user = user(*raw_p[:3])  # first: name, address, interests
        participants.append(new_user)
    names = [p.name for p in participants]
    for i, raw_p in enumerate(raw_data):  # now, get the nice / naughty lists
        is_name_given_in_list_of_participants(raw_p, names)
        p = participants[i]
        # nice list
        if raw_p[3] == "":
            p.can_draw = copy(names)
            p.can_draw.remove(p.name)  # remove yourself from your list
        else:
            p.can_draw = [s.strip() for s in raw_p[3].split(",")]
            if p.can_draw == [""]:
                p.can_draw = []

        # somewhat_naughty_list
        for to_take_out in [s.strip() for s in raw_p[4].split(",")]:
            if to_take_out == "":
                continue
            if to_take_out in p.can_draw:
                p.can_draw.remove(to_take_out)
    # third pass, for very naughty
    for i, raw_p in enumerate(raw_data):
        p = participants[i]
        for to_take_out in [s.strip() for s in raw_p[5].split(",")]:
            if to_take_out == "":
                continue
            if to_take_out in p.can_draw:
                p.can_draw.remove(to_take_out)
            # also take me out of their list
            them = next(
                (x for x in participants if x.name == to_take_out),
                None,
            )
            if raw_p[0] in them.can_draw:  # if my username is in their list
                them.can_draw.remove(raw_p[0])
    # give everyone a debug value 
    random.shuffle(list_of_debug_values)
    for p in participants:
        p.debug_value = list_of_debug_values.pop()


chain = []

current_user = participants[0]
chain.append(current_user.name)

while len(chain) < len(participants):
    available = [p for p in current_user.can_draw if p not in chain]
    if len(available) == 0:
        print(
            "SOMEHOW GOT REALLY UNLUCKY! MADE SOME ASSIGNMENTS SUCH THAT A "
            "SINGLE PATH CONTAINING ALL USERS CANNOT BE DERIVED. "
            "RERUN THE SCRIPT"
        )
        sys.exit(0)

    choice = random.choice(available)
    current_user = next(
        (x for x in participants if x.name == choice),
        None,
    )
    print(f"{current_user.debug_value} ->", end="")
    chain.append(current_user.name)

# making sure first user is in the whitelist of the last user
if (
    chain[0]
    not in next(
        (x for x in participants if x.name == choice),
        None,
    ).can_draw
):
    print("FIRST PERSON WASNT IN WHITELIST OF LAST PERSON, RERUN THE SCRIPT")
    sys.exit(0)

# generating all the txt files:
for i in range(len(chain) - 1):
    next_user = next(
        (x for x in participants if x.name == chain[i + 1]),
        None,
    )
    current_user = next(
        (x for x in participants if x.name == chain[i]),
        None,
    )
    write_to_file(chain[i] + ".txt", current_user, next_user)

# last person
next_user = next(
    (x for x in participants if x.name == chain[0]),
    None,
)
current_user = next(
    (x for x in participants if x.name == chain[-1]),
    None,
)

print(f"{next_user.debug_value}")
write_to_file(chain[-1] + ".txt", current_user, next_user)

print("Done!")
